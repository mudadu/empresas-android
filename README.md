![N|Solid](logo_ioasys.png)

# README #

Estes documento README tem como objetivo responder as perguntas proposta no documento OLDREADME.md

### Biliotecas ###

* A biblioteca com.miguelcatalan:materialsearchview:1.4.0 foi adicionada para criar uma barra de pesquisa mais fiel ao exemplificado no modelo do site zeplin

### Tempo ###

* Caso tivesse mais tempo procuraria descobir mais sobre as conexões da API e do padrão OAuth 2.0 que foi explicitado.
* Alem disso testarias as funcionalidades do Postman

### Instruções ###
* Baixe o apk, dentro da pasta "executavel".
* Para executalo configure o android para liberar a instalação de aplicativos de “fontes desconhecidas”.
* Execute o apk no aparelho android configurado. PS: O aparelho android tem de estrar acima da versão 5.0.
* Para verificar o codigo fonte abra a pasta "IoasysEmpresas" dentro de CodigoFonte na plataforma AndroidStudio.
